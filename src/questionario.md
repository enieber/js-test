## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?

  Clean Code e XP da casa do codigo

2. Quais foram os últimos dois framework javascript que você trabalhou?

  Angularjs e Jasmine para testes

3. Descreva os principais pontos positivos de seu framework favorito.

  Na verdade não tenho nenhum framework favorito, mas o que eu usei bastate foi o angular e ele é muito bom em agilidade, pois ele abstrai muita coisa ficando facil aprende-lo.

4. Descreva os principais pontos negativos do seu framework favorito.

  Os pontos negativos do angular é que ele usa uma 'gambiarra' para ser feito diretivas na sua verção < 2, onde não tinha sido implementado o padrão de web components e todos seus derivados.

5. O que é código de qualidade para você?

  Codigo que tenha passado por varias analises e testes no qual a porcentagem de bugs em produção é minima.


## Conhecimento de desenvolvimento

1. O que é GIT?
  
  Sistema de versionador, utilizado para escrever o kernel do linux.

2. Descreva um workflow simples de trabalho utilizando GIT.

  Se existir um projeto ja em andamento e versionado corretamente, é feito um clone para sua maquina onde você modificara oque é preciso e adiciona ao modo de commit, podendo adicionar outros arquivos antes de commitar, apos commitar é necessario subir ao servidor que dependendo do workflow do projeto tera que ser dado o push a uma branch antes de fazer o pull request para a branch de desenvolvimento.

3. Como você avalia seu grau de conhecimento em Orientação a objeto?

  Não muito grande, mas conheço um pouco dos seus principios básicos como ecapsulamento e herança.

4. O que é Dependency Injection?

  É uma forma de se passar dados de uma classe para outra de maneira a injetar dentro de uma classe qual suas dependencias.

5. O significa a sigla S.O.L.I.D?

  Um dos principios para deixar o design do software mais facil de uso e manutenção. S é Single Responsability, ou seja cada classe deve ter apenas uma reponsabilidade. O é Open/closed uma classe deve ser aberta para extenção, mas fechada para modificação. L é o principio de substituição de Liskov, adimito não entender muito bem essa parte, mas pelo que entendi é que você pode substituir uma classe filha sem muita dificuldade por outra classe semelhante.

6. O que é BDD e qual sua importância para o processo de desenvolvimento?

  Desenvolvimento Orientado a Comportamento. Desenvolver algo atravéz de comportamento de usuarios, ou seja, através de uma história de usuario. Esse processo de desenvolvimento visa desenvolver o minimo possivel atendendo o que o usuário precisa e não o que ele acha que pode precisar, além de trazer o beneficio de desenvolver só oque se vai usar, o tempo para desenvolver uma historia fica bem menor e fazendo com que o tempo de entrega seja bem curto se comparado ao tempo de se desenvolver um projeto robusto com tudo o que o usuario quer.

7. Fale sobre Design Patterns.
  
  Design patterns são soluções comuns para problemas comuns, é uma especie de desenho de um padrão, não quer dizer que o design pattern X vai resolver seu problema de design, apenas quer dizer que ele pode te ajudar a modelar melhor um problema semelhante e ajudar a encontrar uma solução.


9. Discorra sobre práticas que você considera boas no desenvolvimento javascript.

  O não usar sem a real necessidade o DOM.
  Evitar o uso de variaveis globais.
  Usar Strict Model.
  Usar apenas um escopo dentro de funções.

