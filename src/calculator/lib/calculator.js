'use strict';

let hasVirgula = false;

function button(value) {
  let preValue = getValueInDisplay();
  if(value == 0){
    if(!preValue == '' && preValue == 0){
      return;
    }else{
      return addInDisplay(preValue, value);
    }
  }else{
    if(preValue === 0 ){
      return addInDisplay(value);
    }else{
      return addInDisplay(preValue, value);
    }
  }
}

function buttonOperation(value){
  hasVirgula = false;
  let preValue = getValueInDisplay();

  return addInDisplay(preValue, value);
}

function addInDisplay(preValue, value){
  if(!value){
    return document.getElementById('txt_display').value = preValue;
  }else{
    return document.getElementById('txt_display').value = preValue + value;
  }
}

function getValueInDisplay(){
  return document.getElementById('txt_display').value;
}

function clean() {
    hasVirgula = false;
    return addInDisplay('');
}

function virgula() {
  let display = getValueInDisplay();

  if(!display <= 0 && !hasVirgula){
    hasVirgula = true;
    return addInDisplay(display, '.');
  }
}

function equals(){
  hasVirgula = false;
  let op = eval(getValueInDisplay());
  return addInDisplay(op);
}
